from requests import get, post
from getpass import getpass
from sys import argv

if __name__ == '__main__':

    try:
        arg = argv[1].lower()
    except:
        arg = None

    if arg == "register":
        username = input('\nEnter your username:\n').lower()
        email = input('\nEnter your email address:\n').lower()
        password = getpass(prompt='\nEnter your password:\n')
        payload = {
            'username': username,
            'email': email,
            'password': password
        }
        url = 'http://sc18sd.pythonanywhere.com/register'
        print(post(url, data=payload).text)


    elif arg == "login":
        username = input('\nEnter your username:\n').lower()
        password = getpass(prompt='\nEnter your password:\n')
        payload = {
            'username': username,
            'password': password
        }
        url = 'https://sc18sd.pythonanywhere.com/login'
        print(post(url, data=payload).text)


    elif arg == "logout":
        url = 'https://sc18sd.pythonanywhere.com/logout'
        print(get(url).text)


    elif arg == "list":
        url = 'https://sc18sd.pythonanywhere.com/list'
        print(get(url).text)


    elif arg == "view":
        url = 'https://sc18sd.pythonanywhere.com/view'
        print(get(url).text)


    elif arg == "average":
        try:
            url = 'https://sc18sd.pythonanywhere.com/average/' + argv[2] + '/' + argv[3]
            print(get(url).text)
        except:
            print('\nUsage:\npy client.py average <professor_id> <module_code>\n')


    elif arg == "rate":
        try:
            url = 'https://sc18sd.pythonanywhere.com/rate/'+argv[2]+'/'+argv[3]+'/'+argv[4]+'/'+argv[5]+'/'+argv[6]
            print(get(url).text)
        except:
            print('\nUsage:\npy client.py rate <professor_id> <module_code> <year> <semester> <rating>\n')
   
    else:
        print('\nValid arguments:\nregister\nlogin\nlogout\nlist\nview\naverage\nrate\n')
