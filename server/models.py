from django.db import models


class Module(models.Model):
    code = models.CharField(max_length=5)
    name = models.CharField(max_length=50)
    year = models.IntegerField()
    semester = models.IntegerField()


class Professor(models.Model):
    code = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=50)
    modules = models.ManyToManyField(Module)


class Rating(models.Model):
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    rating = models.IntegerField()