"""ratemyprof URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from math import ceil
from django.contrib import admin
from django.urls import path
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from server.models import Module, Professor, Rating


global user
user = None

@csrf_exempt
def register(request):
    global user
    try:
        user = User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password'])
        user.save()
        return HttpResponse('\nSuccessfully registered.\n')
    except:
        return HttpResponse('\nError registering user.\n')
    

@csrf_exempt
def login(request):
    global user
    user = authenticate(username=request.POST['username'], password=request.POST['password'])
    if user:
        return HttpResponse('\nLogged in.\n')
    else:
        return HttpResponse('\nError logging in.\n')


@csrf_exempt
def logout(request):
    global user
    user = None
    return HttpResponse('\nLogged out.\n')


def list(request):
    modules = Module.objects.all()
    infos = ''
    for m in modules:
        professors = Professor.objects.filter(modules__in=[m.id])
        info = [m.code, m.name, m.year, m.semester, []]
        if professors:
            for p in professors:
                info[4].append([p.code, p.name])
        infos += '\n' + info[1] + ' (' + info[0] + ') ' + str(info[2]) + '/S' + str(info[3]) + ': '
        for i in info[4]:
            infos += '\n' + i[1] + ' (' + i[0] + ')'
        infos += '\n'
    return HttpResponse(infos)
        

def avg(professor_id, module_code=None):
    p = Professor.objects.get(code=professor_id)
    scores = []
    if module_code:
        ratings = Rating.objects.filter(professor__code=p.code, module__code=module_code)
    else:
        ratings = Rating.objects.filter(professor__code=p.code)
    for r in ratings:
        scores.append(r.rating)

    avg = sum(scores) / len(scores)
    if avg % 1 >= 0.5:
        avg = ceil(avg)
    else:
        avg = round(avg)
    return [p.code, p.name, avg]


def view(request):
    professors = Professor.objects.all()
    infos = ''
    for p in professors:
        info = avg(p.code)
        infos += '\n' + info[1] + ' (' + info[0] + ')\'s overall rating is: ' + info[2]*'*' + '\n'
    return HttpResponse(infos)


def average(request, professor_id, module_code):
    a = avg(professor_id, module_code)
    module = Module.objects.filter(code=module_code)
    info = '\n'+a[1]+' ('+a[0]+')\'s average rating for '+module[0].name+' ('+module_code+') is '+a[2]*'*'+'\n'
    return HttpResponse(info)


def rate(request, professor_id, module_code, year, semester, rating):
    global user
    if user:
        try:
            professor = Professor.objects.get(code=professor_id)
            module = Module.objects.get(code=module_code, year=year, semester=semester)
            new_rating = Rating(professor=professor, module=module, rating=rating)
            new_rating.save()
            return HttpResponse('\nRating left successfully.\n')
        except:
            return HttpResponse('\nError leaving rating.\n')
    else:
        return HttpResponse('\nPlease login before leaving ratings.\n')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('register', register),
    path('login', login),
    path('logout', logout),
    path('list', list),
    path('view', view),
    path('average/<str:professor_id>/<str:module_code>', average),
    path('rate/<str:professor_id>/<str:module_code>/<int:year>/<int:semester>/<int:rating>', rate)
]
